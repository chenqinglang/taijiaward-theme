<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ MAIN _______________________ -->
  <div class="corner-bot-left">
  <div class="corner-bot-right">
  <div id="main" class="clearfix">

    <div id="content">
      <div id="content-inner" class="inner column center">

        <?php if ($breadcrumb || $title|| $messages || $tabs || $action_links): ?>
        <div id="content-header">
          <div class="head-row1">
            <div class="col1">
              <div class="pr-menu">
                <ul class="links primary-links">
              </ul>
            </div>
            </div>
            <div class="col2">
              <!-- <a href="/node/12">联系我们</a> -->
            </div>
          </div>
          <div class="head-row2">
            <!-- <h2>表演 传承 <br />&nbsp;&nbsp; 理论 传播</h2> -->
          </div>
          <div class="head-row3">
            <?php if ($page['highlight']): ?>
              <div id="highlight"><?php print render($page['highlight']) ?></div>
            <?php endif; ?>
          </div>          
                   
        </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
        <?php print $breadcrumb; ?>
        <?php print render($page['help']); ?>
          <?php if ($title): ?>
            <h1 class="title"><?php //print $title; ?></h1>
          <?php endif; ?>

          <?php print render($title_suffix); ?>
          <?php print $messages; ?>
          
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>

          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
          
          <?php print render($page['content']) ?>
        </div>

        <?php //print $feed_icons; ?>

      </div>
    </div> <!-- /content-inner /content -->

    <?php if ($main_menu || $secondary_menu): ?>
      <div id="navigation" class="menu <?php if (!empty($main_menu)) {print "with-primary";} if (!empty($secondary_menu)) {print " with-secondary";} ?>">
        <?php //print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('links', 'clearfix', 'main-menu')))); ?>
        <?php //print theme('links', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary', 'class' => array('links', 'clearfix', 'sub-menu')))); ?>
      </div>
    <?php endif; ?>

    <?php if ($site_name): ?>
      <?php if ($title): ?>
        <div id="site-name">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">太极传统音乐奖</a>
        </div>
      <?php else: /* Use h1 when the content title is empty */ ?>
        <h1 id="site-name">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
        </h1>
      <?php endif; ?>
    <?php endif; ?>
    
    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column sidebar first">
        <div id="sidebar-first-inner" class="inner">
          <?php print render($page['sidebar_first']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-first -->

    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-second -->

  </div> <!-- /main -->
    </div>
    </div>
  <!-- ______________________ FOOTER _______________________ -->

  <?php if ($page['footer']): ?>
    <div id="footer">
      <?php print render($page['footer']); ?>
    </div> <!-- /footer -->
  <?php endif; ?>
  <div id="footer">
    <div class="copy">版权所有 Copyright, 2012  
      电话: 86(0)10-64887565  电邮：taijiaward@163.com
      北京朝阳区安翔路一号 中国音乐学院 邮编：100101</div>
  </div>
</div> <!-- /page -->
